/*
!(i) 
Код попадает в итоговый файл, только когда вызвана функция, например FLSFunctions.spollers();
Или когда импортирован весь файл, например import "files/script.js";
Неиспользуемый (не вызванный) код в итоговый файл не попадает.

Если мы хотим добавить модуль следует его расскоментировать
*/
import {
  isWebp,
  headerFixed,
  togglePopupWindows,
  addTouchClass,
  addLoadedClass,
  menuInit,
} from './modules'


const burger = document.querySelector('.burger-menu');
const menu = document.querySelector('.header-menu');

function handlerBurger() {
  burger.classList.toggle('active');
  menu.classList.toggle('active');
}

burger.addEventListener('click', handlerBurger);
